package be.codekata;

public class Fizzbuzz {
    public String evaluate(int input) {
//        if (isBuzz(input) && isFizz(input))
//            return "fizzbuzz";
//        if (isFizz(input))
//            return "fizz";
//        else if (isBuzz(input))
//            return "buzz";
//        return input + "";
        return isFizzBuzzable(input) ? "fizzbuzz" : isBuzzable(input) ? "buzz" : isFizzable(input) ? "fizz" : input + "";
    }

    private boolean isFizzBuzzable(int input) {
        return isBuzzable(input) && isFizzable(input);
    }

    private boolean isBuzzable(int input) {
        return input % 5 == 0;
    }

    private boolean isFizzable(int input) {
        return input % 3 == 0;
    }
}
