package be.codekata;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FizzBuzzTests {

    @Test
    void shouldReturn1For1() {
        assertEvaluate("1", 1);
    }

    @Test
    void shouldReturn2For2() {
        assertEvaluate("2", 2);
    }

    @Test
    void shouldReturnFizzFor3() {
        assertEvaluate("fizz", 3);
    }

    @Test
    void shouldReturn4For4() {
        assertEvaluate("4", 4);
    }

    @Test
    void shouldReturnBuzz5For5() {
        assertEvaluate("buzz", 5);
    }

    @Test
    void shouldReturnFizzFor6() {
        assertEvaluate("fizz", 6);
    }

    @Test
    void shouldReturnBuzzFor10() {
        assertEvaluate("buzz", 10);
    }

    @Test
    void shouldReturnFizzbuzzFor15() {
        assertEvaluate("fizzbuzz", 15);
    }

    @Test
    void shouldReturnFizzbuzzFor30() {
        assertEvaluate("fizzbuzz", 30);
    }

    private void assertEvaluate(String expected, int input) {
        Fizzbuzz fizzbuzz = new Fizzbuzz();
        assertEquals(expected, fizzbuzz.evaluate(input));
    }
}
